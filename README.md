#  **低仿腾讯QQ** 

###  介绍

  
   一个使用Android Studio开发的低仿腾讯QQ软件。集成了  **百度地图定位** +  **天气预报** +  **动画插件** +  **安卓新特性** 的演示软件，因该项目是在大二（2018）时制作，现在已废弃一年多，分享给大家作为实例参考，有兴趣的码友可自行pull下来二次开发。

   
#### 软件架构
  基于AS，jdk9的，兼容andorid21（5.1）以上版本。


#### 安装教程

  pull下项目后使用安卓Studio Android引入该项目，等待gradle自动构建完成后，在com.example.qq.qidong.qidong启动即可
 
 ![输入图片说明](https://images.gitee.com/uploads/images/2019/1230/224926_da478c1b_1682497.png "Start")


 **免责声明:** 
   - 该项目仅供学习使用，切勿用以商业用途，其产生的责任后果与本人无关！
   - 该项目仅供学习使用，切勿用以商业用途，其产生的责任后果与本人无关！
   - 该项目仅供学习使用，切勿用以商业用途，其产生的责任后果与本人无关！

   

###   有任何疑问或交流

      

###  QQ：1416360754

### Email：1416360754@qq.com



 

### **部分截图欣赏** 

![输入图片说明](https://images.gitee.com/uploads/images/2019/1230/225518_fb93db84_1682497.jpeg "在这里输入图片标题")![输入图片说明](https://images.gitee.com/uploads/images/2019/1230/225525_271220fb_1682497.jpeg "在这里输入图片标题")![![输入图片说明](http://www.jiayou.art/q4.jpg "在这里输入图片标题")](https://images.gitee.com/uploads/images/2019/1230/225529_60abed7a_1682497.jpeg "在这里输入图片标题")
![输入图片说明](https://images.gitee.com/uploads/images/2019/1230/225848_f35165b3_1682497.jpeg "在这里输入图片标题")![输入图片说明](https://images.gitee.com/uploads/images/2019/1230/225853_7e9f4f3e_1682497.jpeg "在这里输入图片标题")![输入图片说明](https://images.gitee.com/uploads/images/2019/1230/225858_0ead8fa9_1682497.jpeg "在这里输入图片标题")![输入图片说明](https://images.gitee.com/uploads/images/2019/1230/225902_79ee2bb8_1682497.jpeg "在这里输入图片标题")![输入图片说明](https://images.gitee.com/uploads/images/2019/1230/225907_49eeac3b_1682497.jpeg "在这里输入图片标题")![输入图片说明](https://images.gitee.com/uploads/images/2019/1230/225912_4bd77ce4_1682497.jpeg "在这里输入图片标题")![输入图片说明](https://images.gitee.com/uploads/images/2019/1230/225915_cc1a504c_1682497.jpeg "在这里输入图片标题")![输入图片说明](https://images.gitee.com/uploads/images/2019/1230/225918_ae6a2782_1682497.jpeg "在这里输入图片标题")![输入图片说明](https://images.gitee.com/uploads/images/2019/1230/230006_e0ca7bac_1682497.jpeg "在这里输入图片标题")