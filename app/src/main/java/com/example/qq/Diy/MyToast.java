package com.example.qq.Diy;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.qq.R;

/**
 * Created by 烬 on 2018/5/30.
 */

public class MyToast {
    public MyToast() {}
    public static void DiyToast(Context context,String txt)
    {
        Toast customToast = new Toast(context);
        //获得view的布局
        View customView = LayoutInflater.from(context).inflate(R.layout.diytoast,null);
        ImageView img = (ImageView) customView.findViewById(R.id.toast_image);
        TextView tv = (TextView) customView.findViewById(R.id.toast_text);
        //设置ImageView的图片
        img.setBackgroundResource(R.drawable.ic);
        //设置textView中的文字
        tv.setText(txt);
        //设置toast的View,Duration,Gravity最后显示
        customToast.setView(customView);
        customToast.setDuration(Toast.LENGTH_SHORT);
//                customToast.setGravity(Gravity.BOTTOM,100,250);
        customToast.show();
    }
    public static void DiyToast(Context context,String txt,int imageId)
    {
        Toast customToast = new Toast(context);
        //获得view的布局
        View customView = LayoutInflater.from(context).inflate(R.layout.diytoast,null);
        ImageView img = (ImageView) customView.findViewById(R.id.toast_image);
        TextView tv = (TextView) customView.findViewById(R.id.toast_text);
        //设置ImageView的图片
        img.setBackgroundResource(imageId);
        //设置textView中的文字
        tv.setText(txt);
        //设置toast的View,Duration,Gravity最后显示
        customToast.setView(customView);
        customToast.setDuration(Toast.LENGTH_SHORT);
//                customToast.setGravity(Gravity.BOTTOM,100,250);
        customToast.show();
    }
}
