package com.example.qq.download;

import android.Manifest;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.qq.R;

public class xiazai extends AppCompatActivity implements View.OnClickListener {
     public String www;
    EditText editText111;
     private DownloadService.DownloadBinder downloadBinder;
     private ServiceConnection connection = new ServiceConnection() {
         @Override
         public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
             downloadBinder = (DownloadService.DownloadBinder)iBinder;
         }

         @Override
         public void onServiceDisconnected(ComponentName componentName) {

         }
     };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_xiazai);
        Button start = (Button)findViewById(R.id.A1111);
        start.setOnClickListener(this);
        Button pause =(Button)findViewById(R.id.A2222);
        pause.setOnClickListener(this);
         editText111 = (EditText)findViewById(R.id.wangzhi);
        Button cancel = (Button)findViewById(R.id.A3333);
        cancel.setOnClickListener(this);
        Intent intent = new Intent(this,DownloadService.class);
        startService(intent);
        bindService(intent,connection,BIND_AUTO_CREATE);
        if(ContextCompat.checkSelfPermission(xiazai.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(xiazai.this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},1);
        }

    }

    @Override
    public void onClick(View view) {

        if(downloadBinder==null)
        {
            return;
        }
        switch (view.getId())
        {

            case R.id.A1111:          String url =editText111.getText().toString() ;       downloadBinder.startDownload(url);    break;
            case R.id.A2222:       downloadBinder.pauseDownload(); break;
            case R.id.A3333:        downloadBinder.cancelDownload();   break;
                default:break;
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode)
        {
            case 1:
                if(grantResults.length>0){
                    for (int result : grantResults)
                    {
                        if(result!=PackageManager.PERMISSION_GRANTED){
                            Toast.makeText(this,"拒绝写入权限将无法使用该程序！",Toast.LENGTH_SHORT).show();
                            finish();
                            return;
                        }
                    }

                }else
                {
                    Toast.makeText(this,"发生未知的错误！",Toast.LENGTH_SHORT).show();
                    finish();
                }
                break;

            default:
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
       unbindService(connection);
    }

}
