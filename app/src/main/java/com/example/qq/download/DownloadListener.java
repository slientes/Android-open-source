package com.example.qq.download;

/**
 * Created by 烬 on 2018/4/13.
 */

public interface DownloadListener {
    void onProgress(int progress);
    void onSucess();
    void onFailed();
    void onPause();
    void onCanceled();
}
