package com.example.qq.download;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Binder;
import android.os.Environment;
import android.os.IBinder;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import com.example.qq.R;

import java.io.File;

public class DownloadService extends Service {
    private DownloadTask downloadTask;
    private String downloadUrl;
    public DownloadService() {
    }
    private DownloadListener listener = new DownloadListener() {
        @Override
        public void onProgress(int progress) {
           GG().notify(1,gg("下载中...",progress));
        }

        @Override
        public void onSucess() {
                downloadTask = null;
                stopForeground(true);
            GG().notify(1,gg("下载已完成！",-1));
            Toast.makeText(DownloadService.this,"下载成功！",Toast.LENGTH_SHORT).show();
            zhendong();
        }

        @Override
        public void onFailed() {
               downloadTask = null;
               stopForeground(true);
            GG().notify(1,gg("下载失败！",-1));
            Toast.makeText(DownloadService.this,"下载失败！",Toast.LENGTH_SHORT).show();
            zhendong();

        }

        @Override
        public void onPause() {
                downloadTask = null;
            Toast.makeText(DownloadService.this,"下载已暂停！",Toast.LENGTH_SHORT).show();
            zhendong();
        }

        @Override
        public void onCanceled() {
            downloadTask = null;
            stopForeground(true);
            Toast.makeText(DownloadService.this,"已取消下载！",Toast.LENGTH_SHORT).show();
            zhendong();
        }
    };
   private DownloadBinder mBinder = new DownloadBinder();

    private NotificationManager GG()
    {
        return (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
    }
    private Notification gg(String title,int progress){
        Intent intent = new Intent(this,xiazai.class);
        PendingIntent pi = PendingIntent.getActivity(this,0,intent,0);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setSmallIcon(R.mipmap.ic_launcher);
        builder.setLargeIcon(BitmapFactory.decodeResource(getResources(),R.mipmap.ic_launcher));
        builder.setContentIntent(pi);
        builder.setContentTitle(title);
        if(progress>=0)
        {
            builder.setContentText(progress+"%");
            builder.setProgress(100,progress,false);
        }
        return builder.build();
    }
    @Override
    public IBinder onBind(Intent intent) {
//        // TODO: Return the communication channel to the service.
//        throw new UnsupportedOperationException("Not yet implemented");
        return mBinder;

    }
    class DownloadBinder extends Binder{
        public void startDownload(String uri)
        {
            if(downloadTask==null)
            {
                downloadUrl=uri;
                downloadTask = new DownloadTask(listener);
                downloadTask.execute(downloadUrl);
                startForeground(1,gg("下载中...",0));
                Toast.makeText(DownloadService.this,"下载中...",Toast.LENGTH_SHORT).show();

            }
        }
        public void pauseDownload()
        {
            if(downloadTask!=null)
            {
                downloadTask.pausedDownload();
            }
        }
        public void cancelDownload(){
            if(downloadTask!=null)
            {
                downloadTask.cancelDownload();
            }else {
                if(downloadUrl!=null)
                {
                    String fileName = downloadUrl.substring(downloadUrl.lastIndexOf("/"));
                    String directory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath();
                    File file = new File(directory+fileName);
                    if(file.exists())
                    {
                        file.delete();
                    }
                    GG().cancel(1);
                    stopForeground(true);
                    Toast.makeText(DownloadService.this,"文件已经删除，下载服务已经关闭",Toast.LENGTH_SHORT).show();
                    zhendong();
                }
                else {

                }
            }
        }
    }
    private void zhendong()
    {
        Vibrator vv=(Vibrator) getApplication().getSystemService(Service.VIBRATOR_SERVICE);
        vv.vibrate(500);//震半秒钟
    }
}
