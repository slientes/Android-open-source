package com.example.qq.The_one_message;

import android.support.v4.view.GravityCompat;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.qq.R;

import java.util.List;

/**
 * Created by 烬 on 2018/5/5.
 */

public class QQneirong extends RecyclerView.Adapter<QQneirong.Viewholder> {
    List<Messagee> LL;
    private OnClickItemListener onClickItemListener;

    public void Yichu(int pos)
    {
        LL.remove(pos);
        notifyItemRemoved(pos);
    }
    class Viewholder extends RecyclerView.ViewHolder{
        TextView textView1,textView2,textView19;
        de.hdodenhof.circleimageview.CircleImageView imageView;
        RelativeLayout linearLayout;

        public Viewholder(View view)
        {
            super(view);
            linearLayout=view.findViewById(R.id.zai);
            textView1=view.findViewById(R.id.name);
            textView2=view.findViewById(R.id.xiaoming);
            textView19=view.findViewById(R.id.textView19);
            imageView=view.findViewById(R.id.image);
        }
    }
    @Override
    public QQneirong.Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.qqneirong,parent,false);
        TypedValue typedValue = new TypedValue();
        parent.getContext().getTheme().resolveAttribute(R.attr.selectableItemBackground, typedValue, true);
        view.setBackgroundResource(typedValue.resourceId);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(final QQneirong.Viewholder holder, final int position) {
         Messagee messagee = LL.get(position);
         holder.imageView.setImageResource(messagee.getImageid());
         holder.textView1.setText(messagee.getNN());
         holder.textView2.setText(messagee.getmName());
         holder.textView19.setText(messagee.getTime());
         if(onClickItemListener!=null)
         {
             holder.linearLayout.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     int poss = holder.getAdapterPosition();
                     onClickItemListener.onClick(holder.linearLayout,poss);
                 }
             });
             holder.linearLayout.setOnLongClickListener(new View.OnLongClickListener() {
                 @Override
                 public boolean onLongClick(View v) {
                     int poss =  holder.getAdapterPosition();
                      onClickItemListener.onLongClick(holder.linearLayout,poss);
                     return false;
                 }
             });
         }
    }

    @Override
    public int getItemCount() {
       return LL.size();
    }
    public QQneirong(List<Messagee> k)
    {
        LL =k;

    }
    public interface OnClickItemListener{
        void onClick(View view,int pos);
        void onLongClick(View view,int pos);
    }
    public void setOnClickItemListener(OnClickItemListener onClickItemListener)
    {
        this.onClickItemListener =onClickItemListener;
    }
    //

}
