package com.example.qq.msg;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.qq.Diy.DiydiaLog;
import com.example.qq.Diy.MyButton;
import com.example.qq.R;
import com.example.qq.The_Main_Main_Ui;

import java.util.ArrayList;
import java.util.List;

public class MsgActivity extends AppCompatActivity {
    private List<Msg> msgList = new ArrayList<>();
    private EditText editText;
    private Button button,button1,button44;
    private RecyclerView recyclerView;
    private MsgAdapter msgAdapter;
    private Context context;
    private long exitTime = 0;
    private TextView tt,TT;
    private String Name;
    private DiydiaLog diydiaLog;

    private Vibrator myVibrator;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_msg);
        tt =(TextView)findViewById(R.id.NNAAMMEE);
        TT=(TextView)findViewById(R.id.MMMMMM);
       final   Handler handler = new Handler()
        {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what)
                {
                    case 1: tt.setText(Name);  TT.setText("手机在线-4G"); break;
                    default: break;
                }
            }
        };
        myVibrator = (Vibrator) getSystemService(Service.VIBRATOR_SERVICE);
        Intent intent =getIntent();
        Name = intent.getStringExtra("NAME");
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message message =new Message();
                message.what=1;
                handler.sendMessage(message);
            }
        }).start();
        editText =(EditText)findViewById(R.id.Aaaaa);
        button = (Button)findViewById(R.id.send);
        recyclerView = (RecyclerView)findViewById(R.id.Msgg);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        msgAdapter = new MsgAdapter(msgList,context);
        recyclerView.setAdapter(msgAdapter);
  editText.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
      }

      @Override
      public void afterTextChanged(Editable s) {
          int n =editText.getText().length();
          if(n>0)
          {
              button.setBackgroundColor(Color.parseColor("#7EC0EE"));
          }
          else
          button.setBackgroundColor(Color.parseColor("#ffffff"));
      }
  });
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String s = editText.getText().toString();
                if(!"".equals(s))
                {
                    Msg msg2 = new Msg(s,Msg.type_sed);
                    msgList.add(msg2);
                    msgAdapter.notifyItemInserted(msgList.size()-1);
                    recyclerView.scrollToPosition(msgList.size()-1);
                    editText.setText("");
                }
                editText.setImeOptions(EditorInfo.IME_ACTION_UNSPECIFIED);
//                editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//                    @Override
//                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                        //此处会响应2次 分别为ACTION_DOWN和ACTION_UP
//                        if (event.getKeyCode() == KeyEvent.KEYCODE_ENTER &&
//                                event.getAction() == KeyEvent.ACTION_DOWN) {
//
//                            return true;
//                        }
//                        return false;
//                    }
//                });
            }
        });
      Button floatingActionButton = findViewById(R.id.FFFF);
       floatingActionButton.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
//               linearLayoutManager.scrollToPositionWithOffset(0, 0);
//               linearLayoutManager.setStackFromEnd(true);
               Toast.makeText(getApplicationContext(),"敬请期待",Toast.LENGTH_SHORT).show();
           }
       });
       button44=(Button)findViewById(R.id.BACK);
       button44.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
            finish();
           }
       });

        msgAdapter.setOnItemClickListener(new MsgAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onItemClick(View view, int data) {
                Msg msg =msgList.get(data);
                String n = msg.getneirong().toString();
                int Data = data+1;
                Toast.makeText(MsgActivity.this,"这是第"+ Data +"个消息\n" + "内容：" + n,Toast.LENGTH_LONG).show();
            }
        });
        msgAdapter.setOnItemLongClickListener(new MsgAdapter.OnRecyclerItemLongListener() {
            @Override
            public void onItemLongClick(View view, final int position) {
                myVibrator.vibrate(50);
                diydiaLog =new DiydiaLog(MsgActivity.this);
               diydiaLog.setTitle("Warning");
                diydiaLog.setMessage("确认删除该条记录吗？");
               diydiaLog.setYesOnclickListener("确定", new DiydiaLog.onYesOnclickListener() {
                    @Override
                    public void onYesClick() {
                        msgAdapter.yichu(position);
                        diydiaLog.dismiss();
                    }
                });
                diydiaLog.setNoOnclickListener("取消", new DiydiaLog.onNoOnclickListener() {
                    @Override
                    public void onNoClick() {
                        diydiaLog.dismiss();
                    }
                });
                diydiaLog.show();
            }
        });

            }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        finish();
        return true;
    }

  public void f()
  {

      this.finish();
  }

private void showwww()
{
    AlertDialog.Builder builder1 = new AlertDialog.Builder(MsgActivity.this);
    builder1.setTitle("温馨提示");
    builder1.setMessage("退出即销毁所有聊天记录，Are You Sure？");
    builder1.setCancelable(false);
    builder1.setPositiveButton("我非常确定", new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            f();
        }
    });
    builder1.setNegativeButton("我啊么想好", new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            Toast.makeText(MsgActivity.this,"七宗罪：傲慢、妒忌、暴怒、懒惰、贪婪、贪食、色欲。",Toast.LENGTH_LONG).show();
        }
    });
    builder1.show();
}

}
