package com.example.qq.msg;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.qq.R;

import java.util.List;

import static android.content.Context.CLIPBOARD_SERVICE;

/**
 * Created by 烬 on 2018/4/23.
 */

public class MsgAdapter extends RecyclerView.Adapter<MsgAdapter.Viewholder> {
    private List<Msg> list;
    public Context context;
    public static OnRecyclerViewItemClickListener mOnItemClickListener = null;
    public static OnRecyclerItemLongListener mOnItemLong = null;

    public void yichu(int pos)
    {
        list.remove(pos);
        notifyItemRemoved(pos);
    }
    @Override
    public Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {
      View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.msg_item,parent,false);
        TypedValue typedValue = new TypedValue();
        parent.getContext().getTheme().resolveAttribute(R.attr.selectableItemBackground, typedValue, true);
        view.setBackgroundResource(typedValue.resourceId);
        Viewholder viewholder = new Viewholder(view, mOnItemClickListener,mOnItemLong);
      return viewholder;
    }

    @Override
    public void onBindViewHolder(final Viewholder holder, int position) {
         Msg msg = list.get(position);
         if(msg.gettype()==Msg.type_rec)
         {
             holder.left.setVisibility(View.VISIBLE);
             holder.right.setVisibility(View.GONE);
             holder.leftMsg.setText(msg.getneirong());
         }
         if(msg.gettype()==Msg.type_sed)
         {
             holder.right.setVisibility(View.VISIBLE);
             holder.left.setVisibility(View.GONE);
             holder.rightMsg.setText(msg.getneirong());
         }

    }

    @Override
    public int getItemCount() {
       return list.size();
    }

    static class Viewholder extends RecyclerView.ViewHolder implements View.OnClickListener,View.OnLongClickListener{
          LinearLayout left;
          LinearLayout right;
          TextView leftMsg;
          TextView rightMsg;
          View fruitview;
        de.hdodenhof.circleimageview.CircleImageView de;
        private OnRecyclerViewItemClickListener mOnItemClickListener = null;
        private OnRecyclerItemLongListener mOnItemLong = null;



          public Viewholder(View view, OnRecyclerViewItemClickListener mListener,OnRecyclerItemLongListener longListener)
          {

              super(view);
              this.mOnItemClickListener = mListener;
              this.mOnItemLong = longListener;
              left = (LinearLayout)view.findViewById(R.id.lefelayout);
              right =(LinearLayout)view.findViewById(R.id.rightlayout);
              leftMsg=(TextView)view.findViewById(R.id.left_);
              rightMsg=(TextView)view.findViewById(R.id.right_);
              de=(de.hdodenhof.circleimageview.CircleImageView)view.findViewById(R.id.touxiang);
              fruitview = view;
              view.setOnClickListener(this);
              view.setOnLongClickListener(this);

          }

        @Override
        public void onClick(View v) {
            if (mOnItemClickListener != null) {
                //注意这里使用getTag方法获取数据
                mOnItemClickListener.onItemClick(v, getAdapterPosition());
            }
        }

        @Override
        public boolean onLongClick(View v) {
            if(mOnItemLong != null){
                mOnItemLong.onItemLongClick(v,getPosition());
            }
            return true;
        }
    }
      public MsgAdapter(List<Msg> list,Context context)
      {
          this.list = list;
          this.context =context;
//         mClipboardManager = (ClipboardManager) context.getSystemService(CLIPBOARD_SERVICE);
      }

    public interface OnRecyclerViewItemClickListener {
        void onItemClick(View view, int data);

    }
    public interface OnRecyclerItemLongListener{
        void onItemLongClick(View view,int position);
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }
    public void setOnItemLongClickListener(OnRecyclerItemLongListener listener){
        this.mOnItemLong =  listener;
    }
}


