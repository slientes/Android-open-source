package com.example.qq.The_seconde_view;

/**
 * Created by Jay on 2015/9/25 0025.
 */
public class Item {
    private String NName;
    private int iId;
    private String iName;

    public Item() {
    }

    public Item(int iId, String iName,String NName) {
        this.iId = iId;
        this.iName = iName;
        this.NName =NName;
    }

    public int getiId() {
        return iId;
    }

    public String getiName() {
        return iName;
    }

    public void setiId(int iId) {
        this.iId = iId;
    }

    public void setiName(String iName) {
        this.iName = iName;
    }
    public void setNName(String NName){this.NName=NName;}

    public String getNName() {
        return NName;
    }
}
