package com.example.qq.The_seconde_view;

/**
 * Created by Jay on 2015/9/25 0025.
 */
public class Group {
    private String gName;
    private String zaixian;

    public Group() {
    }

    public Group(String gName,String zaixian) {
        this.gName = gName;
        this.zaixian=zaixian;
    }

    public String getgName() {
        return gName;
    }

    public void setgName(String gName) {
        this.gName = gName;
    }

    public String getZaixian() {
        return zaixian;
    }

    public void setZaixian(String zaixian) {
        this.zaixian = zaixian;
    }
}
