package com.example.qq;

import android.Manifest;
import android.app.Activity;
import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v4.widget.DrawerLayout.DrawerListener;

import com.example.qq.Adapter_for_youhu.Myyouhua;
import com.example.qq.Adapter_for_youhu.right_list;
import com.example.qq.Diy.BounceScrollView;
import com.example.qq.Diy.DiydiaLog;
import com.example.qq.Diy.NestedExpandaleListView;
import com.example.qq.DiySwfsh.Diyshuaxin;
import com.example.qq.Diy.MyToast;
import com.example.qq.GesturesLister.RecyclerItemClickListener;
import com.example.qq.H5.MainActivity;
import com.example.qq.Read_People_of_Phone.Phonecx;
import com.example.qq.The_seconde_view.Group;
import com.example.qq.The_seconde_view.Item;
import com.example.qq.The_seconde_view.MyBaseExpandableListAdapter;
import com.example.qq.The_seconde_view.MyPagerAdapter;
import com.example.qq.The_three_adapter.MyThreeAdapter;
import com.example.qq.The_three_adapter.Three;
import com.example.qq.download.xiazai;
import com.example.qq.gps.Gps;
import com.example.qq.msg.MsgActivity;
import com.example.qq.music.Musci;
import com.example.qq.Old_Login.UI;
import com.example.qq.The_one_message.Messagee;
import com.example.qq.The_one_message.QQneirong;
import com.example.qq.Read_People_of_Phone.readdd;
import com.example.qq.setui.ScrollingActivity;
import com.githang.statusbar.StatusBarCompat;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import com.nineoldandroids.view.ViewHelper;

import static android.graphics.Color.RED;

public class The_Main_Main_Ui extends AppCompatActivity implements View.OnClickListener,
        ViewPager.OnPageChangeListener {
  private DrawerLayout drawerLayout;
  private  Intent intent;
  private RecyclerView recyclerView;
  private List<Messagee> lll = new ArrayList<>();
  private SwipeRefreshLayout swipeRefreshLayout;
  private QQneirong qQneirong;
  private static int all = 1;
  private Context mContext;
  private LinearLayout include;
  private LinearLayout l1;
   /////////////The second page view_five var/////
    private ViewPager vpager_four;
    private ImageView img_cursor;
    private TextView tv_one;
    private TextView tv_two;
    private TextView tv_three;
//The sencond page 的动画效果/////////
    private ArrayList<View> listViews;
    private int offset = 0;//移动条图片的偏移量
    private int currIndex = 0;//当前页面的编号
    private int bmpWidth;// 移动条图片的长度
    private int one = 0; //移动条滑动一页的距离
    private int two = 0; //滑动条移动两页的距离
    //可折叠列表(联系人Item)
    private ArrayList<Group> gData = null;
    private ArrayList<ArrayList<Item>> iData = null;
    private ArrayList<Item> lData = null;
    private NestedExpandaleListView xlist_lol;
    private MyBaseExpandableListAdapter myAdapter = null;
    private Diyshuaxin diyshuaxin;
        //用于刷新主UI的消息和更新标题文字
    private Handler handler =new Handler()
    {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what)
            {
                case 1:   SHUAXIN();   Toast.makeText(The_Main_Main_Ui.this, "刷新成功！", Toast.LENGTH_SHORT).show();break;
                case 2:  te.setText("消 息"); break;
                case 3:  te.setText("联系人"); break;
                case 4:  te.setText("动 态");  break;
                case 5: pleace.setText(difang);
                default:  break;
            }
        }
    };
    //用于更新标题
    private TextView  te ;
   //第三页Page的下面的列表
    private List<Three> MMthree =new ArrayList<>();
   //自定义的DIaLog
    private DiydiaLog selfDialog,SelfDialog;
    //振动器服务
    private Vibrator myVibrator;
    //点击头像换头像
    public static final int PHOTO_REQUEST_CAREMA = 3;// 拍照
    public static final int CROP_PHOTO = 2;
    private de.hdodenhof.circleimageview.CircleImageView picture;
    private Uri imageUri;
    public static File tempFile;
//第三界面的滑动View
    private BounceScrollView scrollView;
//右划的Item
    private RecyclerView youhua_item1;
    private List<right_list> Mthree =new ArrayList<>();
    //右侧栏下面的地区
    private TextView pleace;
    private String difang;


    //右划栏的三个图标的点击事件
    private LinearLayout L1,L2,L3;
   //主题颜色
    private int zhiticolor = 2;
    //右划栏的主界面
   private RelativeLayout RR;
   //颜色转换
    int color_3 = Color.parseColor("#8F8F8F");
    //夜间主题文字颜色





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_the__main__main__ui);
        //状态栏颜色设置
        StatusBarCompat.setStatusBarColor(this, Color.WHITE, true);
        //获取当前上下文
        mContext = The_Main_Main_Ui.this;
        //获取振动器的实例
        myVibrator = (Vibrator) getSystemService(Service.VIBRATOR_SERVICE);
        //初始化第三页数据
        ini();
        //右划栏初始化
        inin();
        //获取地区实例
        pleace =(TextView)findViewById(R.id.PPPP);
        //获取可折列表的实例
        diyshuaxin = (Diyshuaxin) findViewById(R.id.shauxin);
        //获取标题实例
        te =(TextView)findViewById(R.id.lianxiren);
        //获取滑动View实例
//        scrollView =(BounceScrollView)findViewById(R.id.diyhuadong);
        //右划布局
        drawerLayout = (DrawerLayout) findViewById(R.id.Drawer);
        //分别为第二个(联系人)第三个(动态)帧布局的View
        include = (LinearLayout) findViewById(R.id.INCLUDE);
        l1 = (LinearLayout) findViewById(R.id.LL);
        //主UI的左上角图标点击打开左侧导航栏
        CircleImageView button7 = (CircleImageView) findViewById(R.id.biaoti_title);
        button7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(GravityCompat.START);
            }
        });
        ///主UI右上角的图标点击打开悬浮按钮
        Button Button8 = (Button) findViewById(R.id.biaoti_title1);
        Button8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initPopWindow(v);
            }
        });
        initEvents();
        picture =(de.hdodenhof.circleimageview.CircleImageView)findViewById(R.id.NEW_ICON);
        picture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                quanxian(mContext);
            }
        });
        //为主UI下面的三个按钮设置功能及其效果
        final RelativeLayout b1 = (RelativeLayout) findViewById(R.id.R1);
        final RelativeLayout b2 = (RelativeLayout) findViewById(R.id.R2);
        final RelativeLayout b3 = (RelativeLayout) findViewById(R.id.R3);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Message message =new Message();
                message.what=2;
                handler.sendMessage(message);
                diyshuaxin.setVisibility(View.VISIBLE);
                include.setVisibility(View.INVISIBLE);
                l1.setVisibility(View.INVISIBLE);
                b1.setBackgroundColor(Color.GRAY);
                b2.setBackgroundColor(Color.WHITE);
                b3.setBackgroundColor(Color.WHITE);

            }
        });
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Message message =new Message();
                message.what=3;
                handler.sendMessage(message);
                diyshuaxin.setVisibility(View.INVISIBLE);
                l1.setVisibility(View.INVISIBLE);
                include.setVisibility(View.VISIBLE);
                l1.setVisibility(View.INVISIBLE);

                b1.setBackgroundColor(Color.WHITE);
                b2.setBackgroundColor(Color.GRAY);
                b3.setBackgroundColor(Color.WHITE);

            }
        });
        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Message message =new Message();
                message.what=4;
                handler.sendMessage(message);
                diyshuaxin.setVisibility(View.INVISIBLE);
                include.setVisibility(View.INVISIBLE);
                l1.setVisibility(View.VISIBLE);

                b1.setBackgroundColor(Color.WHITE);
                b2.setBackgroundColor(Color.WHITE);
                b3.setBackgroundColor(Color.GRAY);
//                scrollView.fullScroll(ScrollView.FOCUS_UP);

            }
        });
        ImageButton Q1 =(ImageButton) findViewById(R.id.AAAA);
        ImageButton Q2 =(ImageButton) findViewById(R.id.BBBB);
        ImageButton Q3 =(ImageButton) findViewById(R.id.cccc);
        Q1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(The_Main_Main_Ui.this,"好友动态",Toast.LENGTH_SHORT).show();
            }
        });
        Q2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(The_Main_Main_Ui.this,"附近的人",Toast.LENGTH_SHORT).show();
            }
        });
        Q3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(The_Main_Main_Ui.this,"兴趣部落",Toast.LENGTH_SHORT).show();
            }
        });
       // 右侧导航栏打开每个Item的功能
//        NavigationView navigationView = findViewById(R.id.nav_view);
//        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
//            @Override
//            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
//                switch (item.getItemId()) {
//                    case R.id.number:
//                        Intent intent000 = new Intent(The_Main_Main_Ui.this, readdd.class);
//                        startActivity(intent000);
//                        break;
//                    case R.id.music:
//                        Intent intent11 = new Intent(The_Main_Main_Ui.this, Musci.class);
//                        startActivity(intent11);
////                        drawerLayout.closeDrawers();
//                        break;
//
//                    case R.id.GPSS:
//                        Intent intent333 = new Intent(The_Main_Main_Ui.this, Gps.class);
//                        startActivity(intent333);
////                        drawerLayout.closeDrawers();
//                        break;
//                    case R.id.Down:
//                        Intent intent444 = new Intent(The_Main_Main_Ui.this, xiazai.class);
//                        startActivity(intent444);
////                        drawerLayout.closeDrawers();
//                        break;
//                    case R.id.huantouxiang:
//                        quanxian(mContext);
//                        break;
//                    default:
//                }
//
//                return true;
//            }
//        });
        //初始化消息界面
        shuaxin();
        //消息列表的适配器
        recyclerView = (RecyclerView) findViewById(R.id.neiRong);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        qQneirong = new QQneirong(lll);
        recyclerView.setAdapter(qQneirong);
        //动画效果
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
       //4 消息列表的点击事件
        qQneirong.setOnClickItemListener(new QQneirong.OnClickItemListener() {
            @Override
            public void onClick(View view, int pos) {
                Messagee messagee = lll.get(pos);
                String string = messagee.getNN().toString();
                Intent intent = new Intent(The_Main_Main_Ui.this, MsgActivity.class);
                intent.putExtra("NAME", string);
                startActivity(intent);
                MyToast.DiyToast(getApplicationContext(), "This is the message of " + (pos + 1));
                //点击消息列表进入对话活动
            }

            //消息列表的长按事件
            @Override
            public void onLongClick(View view, final int pos) {
                myVibrator.vibrate(50);
                SelfDialog =new DiydiaLog(The_Main_Main_Ui.this);
                SelfDialog.setTitle("Warning");
                SelfDialog.setMessage("确认删除该记录吗？");
                SelfDialog.setYesOnclickListener("确定", new DiydiaLog.onYesOnclickListener() {
                    @Override
                    public void onYesClick() {
                        qQneirong.Yichu(pos);
                        SelfDialog.dismiss();
                    }
                });
                SelfDialog.setNoOnclickListener("取消", new DiydiaLog.onNoOnclickListener() {
                    @Override
                    public void onNoClick() {
                       SelfDialog.dismiss();
                    }
                });
                SelfDialog.show();
            }
        });
        //主UI是帧布局，所以得隐藏第二个（联系人）和第三个（动态）布局
        include.setVisibility(View.INVISIBLE);
        l1.setVisibility(View.INVISIBLE);
        b1.setBackgroundColor(Color.GRAY);

        //数据准备


        initViews();
        xlist_lol.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                Item item = lData.get(childPosition);
                String name = item.getiName().toString();
                String say =item.getNName().toString();
                int imageid =item.getiId();
                String time ="刚刚";
                Intent intent = new Intent(The_Main_Main_Ui.this, MsgActivity.class);
                intent.putExtra("NAME", name);
                lll.add(new Messagee(name,imageid,suiji(),time));
                qQneirong.notifyDataSetChanged();
                startActivity(intent);
                return true;
            }
        });
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////可折叠列表的适配器以及数据///////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        diyshuaxin.setOnRefreshListener(
                new Diyshuaxin.OnCircleRefreshListener() {
                    @Override
                    public void refreshing() {
                        Toast.makeText(The_Main_Main_Ui.this, "正在刷新...", Toast.LENGTH_SHORT).show();
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    Thread.sleep(3000);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                Message message= new Message();
                                message.what=1;
                                handler.sendMessage(message);
                                diyshuaxin.finishRefreshing();
                            }
                        }).start();

                    }

                    @Override
                    public void completeRefresh() {

                    }

                });
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////                        第三页的Adapter适配器以及点击长按效果                  / /////////////////////////////////////////
        ////////////////↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓//////////////////////
        ////////////////↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓///////////////////////

        RecyclerView recyclerView1 =(RecyclerView)findViewById(R.id.recy_cler);
        LinearLayoutManager linearLayoutManager1 =new LinearLayoutManager(this);
        recyclerView1.setLayoutManager(linearLayoutManager1);
        final MyThreeAdapter myThreeAdapter =new MyThreeAdapter(MMthree);
        recyclerView1.setAdapter(myThreeAdapter);
        recyclerView1.setHasFixedSize(true);
        recyclerView1.setNestedScrollingEnabled(false);
        recyclerView1.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));

         //列表的点击事件
      myThreeAdapter.SetOnClickItemListener(new MyThreeAdapter.OnClickItemListener() {
          @Override
          public void onClick(View view, int pos) {
              Three three =MMthree.get(pos);
              String name =three.getName().toString();
              MyToast.DiyToast(getApplicationContext(), name);
          }

          @Override
          public void onLongClick(View view, final int pos) {
              myVibrator.vibrate(50);
              Three three =MMthree.get(pos);
              String name =three.getName().toString();
              SelfDialog =new DiydiaLog(The_Main_Main_Ui.this);
              SelfDialog.setTitle("Warning");
              SelfDialog.setMessage("确认删除  "+  "\""  +  name +"\""  +"  该项吗？");
              SelfDialog.setYesOnclickListener("确定", new DiydiaLog.onYesOnclickListener() {
                  @Override
                  public void onYesClick() {
                      myThreeAdapter.Yichu(pos);
                      SelfDialog.dismiss();
                  }
              });
              SelfDialog.setNoOnclickListener("取消", new DiydiaLog.onNoOnclickListener() {
                  @Override
                  public void onNoClick() {
                      SelfDialog.dismiss();
                  }
              });
              SelfDialog.show();
          }
      });

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////第三页的Adapter适配器以及点击长按效果////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
       youhua_item1 =(RecyclerView)findViewById(R.id.youhua_it);
        LinearLayoutManager linearLayoutManager11 =new LinearLayoutManager(this);
        youhua_item1.setLayoutManager(linearLayoutManager11);
        Myyouhua myyouhua = new Myyouhua(Mthree);
        youhua_item1.setAdapter(myyouhua);
        youhua_item1.setHasFixedSize(true);
        youhua_item1.setNestedScrollingEnabled(false);
        myyouhua.SetOnClickItemListener(new Myyouhua.OnClickItemListener() {
            @Override
            public void onClick(View view, int pos) {
                right_list right_list111 = Mthree.get(pos);
                String Name =right_list111.getName().toString();
                switch (Name)
                {
                    case "天  气":Intent intent =new Intent(The_Main_Main_Ui.this,Gps.class);startActivityForResult(intent,1); break;
                    case "极简下载器": startActivity(new Intent(The_Main_Main_Ui.this,xiazai.class)); break;
                    case "获取本机联系人":startActivity(new Intent(The_Main_Main_Ui.this,Phonecx.class)); break;
                    case "极简音频播放器":startActivity(new Intent(The_Main_Main_Ui.this,Musci.class)); break;
                    case "远程打印":startActivity(new Intent(The_Main_Main_Ui.this, MainActivity.class)); break;
                    default: Toast.makeText(mContext,"敬请期待",Toast.LENGTH_SHORT).show(); break;
                }
            }
        });
        //消息列表的内容
//        View view1 = LayoutInflater.from(mContext).inflate(R.layout.qqneirong, null, false);
//        textView1=view1.findViewById(R.id.name);
//       textView2=view1.findViewById(R.id.xiaoming);
//        textView19=view1.findViewById(R.id.textView19);
        RR = (RelativeLayout)findViewById(R.id.pp);
        //三个图标的点击事件
        L1=(LinearLayout)findViewById(R.id.QAZ);
        L2=(LinearLayout)findViewById(R.id.QAX);
        L3=(LinearLayout)findViewById(R.id.QAC);
        L1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             startActivity(new Intent(The_Main_Main_Ui.this, ScrollingActivity.class));
            }
        });
        L2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if(zhiticolor%2==0)
               {
                   RR.setBackgroundResource(R.drawable.anse);
                   diyshuaxin.setBackgroundColor(Color.parseColor("#333444"));
//                   recyclerView.setBackgroundColor(Color.parseColor("#333444"));
//                   include.setBackgroundColor(Color.parseColor("#333444"));

//                   textView1.setTextColor(Color.parseColor("#FCFCFC"));
//                   textView2.setTextColor(Color.parseColor("#FCFCFC"));
//                   textView19.setTextColor(Color.parseColor("#FCFCFC"));
                   zhiticolor++;
               }
               else {
                   RR.setBackgroundResource(R.drawable.bingshan);
                   diyshuaxin.setBackgroundColor(Color.parseColor("#ffffff"));
                   zhiticolor++;
               }

            }
        });
        L3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                  if(pleace.getText().toString().equals("温度"))
                  {
                      DiydiaLog diydiaLog = new DiydiaLog(mContext);
                      diydiaLog =new DiydiaLog(The_Main_Main_Ui.this);
                      diydiaLog.setTitle("温馨提示");
                      diydiaLog.setMessage("你当前还未获取你当前的地理位置，点击确定后点击\"刷新当前地理位置\"可获取你的当前位置（县区)");
                      final DiydiaLog finalDiydiaLog1 = diydiaLog;
                      diydiaLog.setYesOnclickListener("确定", new DiydiaLog.onYesOnclickListener() {
                          @Override
                          public void onYesClick() {
                              Intent intent =new Intent(The_Main_Main_Ui.this,Gps.class);startActivityForResult(intent,1);
                              finalDiydiaLog1.dismiss();
                          }
                      });
                      final DiydiaLog finalDiydiaLog = diydiaLog;
                      diydiaLog.setNoOnclickListener("取消", new DiydiaLog.onNoOnclickListener() {
                          @Override
                          public void onNoClick() {
                              finalDiydiaLog.dismiss();
                          }
                      });
                      diydiaLog.show();
                  }
                  else
                      Toast.makeText(getApplicationContext(),"您当前所在的县区位置："+pleace.getText().toString(),Toast.LENGTH_LONG).show();
                  }
            });

    }

    private void inin() {
        right_list right_list11 = new right_list(R.drawable.gpssss,"天  气");
        Mthree.add(right_list11);
        right_list right_list111 = new right_list(R.drawable.downn,"极简下载器");
        Mthree.add(right_list111);
        right_list right_list1111 = new right_list(R.drawable.people,"获取本机联系人");
        Mthree.add(right_list1111);
        right_list right_list11111 = new right_list(R.drawable.musicc,"极简音频播放器");
        Mthree.add(right_list11111);
        right_list right_list111111 = new right_list(R.drawable.ic,"远程打印");
        Mthree.add(right_list111111);
        right_list right_list2 = new right_list(R.drawable.ic,"Final钱包");
        Mthree.add(right_list2);
        right_list right_list22 = new right_list(R.drawable.ic,"个性装扮");
        Mthree.add(right_list22);


    }

    //随机获取一个消息列表
    private String suiji()
    {
       int num= (int) Math.floor(Math.random()*10+1);
       String[] The_message ={
               ""
               ,"如果你有梦想的话，就要去捍卫它。"
               ,"当你最认为困难的时候，其实就是你最接近成功的时候。"
               , "当人们做不到一些事情的时候，他们就会对你说你也同样不能。","有了目标就要全力以赴。"
               ,"不能对所有的人都一样好，否则你永远都不懂得珍惜。"
               ,"有梦想的人，从来叫醒他的都不是闹钟，而是梦想。"
               ,"幸福里没有什么，只有我。"
               ,"你穷，是因为你没有野心."
               ,"不要等到明天，明天太遥远，今天就行动。"
               ,"晚上想想千条路，早上醒来走原路。"};
        return The_message[num];
    }

    private void ini() {
        Three three1 =new Three(R.drawable.ic,"游戏",R.drawable.jian);
        MMthree.add(three1);
        Three three2 =new Three(R.drawable.ic,"日迹",R.drawable.jian);
        MMthree.add(three2);
        Three three3 =new Three(R.drawable.ic,"看点",R.drawable.jian);
        MMthree.add(three3);
        Three three4 =new Three(R.drawable.ic,"京东购物",R.drawable.jian);
        MMthree.add(three4);
        Three three5 =new Three(R.drawable.ic,"企鹅电竞",R.drawable.jian);
        MMthree.add(three5);
        Three three6 =new Three(R.drawable.ic,"鹅漫U品",R.drawable.jian);
        MMthree.add(three6);
        Three three7 =new Three(R.drawable.ic,"动漫",R.drawable.jian);
        MMthree.add(three7);
        Three three8 =new Three(R.drawable.ic,"阅读",R.drawable.jian);
        MMthree.add(three8);
        Three three9 =new Three(R.drawable.ic,"音乐",R.drawable.jian);
        MMthree.add(three9);
        Three three10 =new Three(R.drawable.ic,"直播",R.drawable.jian);
        MMthree.add(three10);
        Three three11 =new Three(R.drawable.ic,"应用宝",R.drawable.jian);
        MMthree.add(three11);
        Three three12 =new Three(R.drawable.ic,"运动",R.drawable.jian);
        MMthree.add(three12);
        Three three23 =new Three(R.drawable.ic,"吃喝玩乐",R.drawable.jian);
        MMthree.add(three23);
        Three three34 =new Three(R.drawable.ic,"同城服务",R.drawable.jian);
        MMthree.add(three34);
        Three three46 =new Three(R.drawable.ic,"腾讯新闻",R.drawable.jian);
        MMthree.add(three46);
        Three three55 =new Three(R.drawable.ic,"腾讯课堂",R.drawable.jian);
        MMthree.add(three55);
        Three three462 =new Three(R.drawable.ic,"企鹅辅导",R.drawable.jian);
        MMthree.add(three462);
        Three three4622 =new Three(R.drawable.ic,"企鹅辅导",R.drawable.jian);
        MMthree.add(three4622);
        Three three4623 =new Three(R.drawable.ic,"企鹅辅导",R.drawable.jian);
        MMthree.add(three4623);
        Three three46232 =new Three(R.drawable.ic,"企鹅辅导",R.drawable.jian);
        MMthree.add(three46232);
        MMthree.add(three46232);
        MMthree.add(three46232);
        MMthree.add(three46232);
        MMthree.add(three46232);
        MMthree.add(three46232);
        MMthree.add(three46232);  MMthree.add(three46232);
        MMthree.add(three46232);  MMthree.add(three46232);  MMthree.add(three46232);  MMthree.add(three46232);  MMthree.add(three46232);


    }

    private void shipeiqi()
    {

        //数据准备
        gData = new ArrayList<Group>();
        iData = new ArrayList<ArrayList<Item>>();

        gData.add(new Group("AD","10/24"));
        gData.add(new Group("AP","10/40"));
        gData.add(new Group("TANK","1/5"));
        gData.add(new Group("辅助","15/15"));
        gData.add(new Group("末日人机AI","4/66"));



        //AD组
        lData = new ArrayList<Item>();
        lData.add(new Item(R.mipmap.iv_lol_icon3,"剑圣","[手机在线] 我的剑就是你的剑！"));
        lData.add(new Item(R.mipmap.iv_lol_icon4,"德莱文","[4G在线] 来发子弹怎么样？"));
        lData.add(new Item(R.mipmap.iv_lol_icon13,"男枪","[WiFi在线] 枪火轰鸣！" ));
        lData.add(new Item(R.mipmap.iv_lol_icon14,"韦鲁斯","[离线请留言 我是敌人的梦魇！"));
        lData.add(new Item(R.mipmap.iv_lol_icon10, "龙女","[手机在线]    德玛西亚吾运必彰!"));
        lData.add(new Item(R.mipmap.iv_lol_icon12, "狗熊","[手机在线] 力量与智慧指引着我!"));
        iData.add(lData);


        //AP组
        lData = new ArrayList<Item>();
        lData.add(new Item(R.mipmap.iv_lol_icon3,"剑圣","[手机在线] 我的剑就是你的剑！"));
        lData.add(new Item(R.mipmap.iv_lol_icon4,"德莱文","[4G在线] 来发子弹怎么样？"));
        lData.add(new Item(R.mipmap.iv_lol_icon13,"男枪","[WiFi在线] 枪火轰鸣！" ));
        lData.add(new Item(R.mipmap.iv_lol_icon14,"韦鲁斯","[离线请留言 我是敌人的梦魇！"));
        lData.add(new Item(R.mipmap.iv_lol_icon10, "龙女","[手机在线]    德玛西亚吾运必彰!"));
        lData.add(new Item(R.mipmap.iv_lol_icon12, "狗熊","[手机在线] 力量与智慧指引着我!"));
        iData.add(lData);


        //TANK组
        lData = new ArrayList<Item>();
        lData.add(new Item(R.mipmap.iv_lol_icon3,"剑圣","[手机在线] 我的剑就是你的剑！"));
        lData.add(new Item(R.mipmap.iv_lol_icon4,"德莱文","[4G在线] 来发子弹怎么样？"));
        lData.add(new Item(R.mipmap.iv_lol_icon13,"男枪","[WiFi在线] 枪火轰鸣！" ));
        lData.add(new Item(R.mipmap.iv_lol_icon14,"韦鲁斯","[离线请留言 我是敌人的梦魇！"));
        lData.add(new Item(R.mipmap.iv_lol_icon10, "龙女","[手机在线]    德玛西亚吾运必彰!"));
        lData.add(new Item(R.mipmap.iv_lol_icon12, "狗熊","[手机在线] 力量与智慧指引着我!"));
        iData.add(lData);


        lData =new ArrayList<Item>();
        lData.add(new Item(R.mipmap.iv_lol_icon3,"剑圣","[手机在线] 我的剑就是你的剑！"));
        lData.add(new Item(R.mipmap.iv_lol_icon4,"德莱文","[4G在线] 来发子弹怎么样？"));
        lData.add(new Item(R.mipmap.iv_lol_icon13,"男枪","[WiFi在线] 枪火轰鸣！" ));
        lData.add(new Item(R.mipmap.iv_lol_icon14,"韦鲁斯","[离线请留言 我是敌人的梦魇！"));
        lData.add(new Item(R.mipmap.iv_lol_icon10, "龙女","[手机在线]    德玛西亚吾运必彰!"));
        lData.add(new Item(R.mipmap.iv_lol_icon12, "狗熊","[手机在线] 力量与智慧指引着我!"));
        iData.add(lData);


        lData =new ArrayList<Item>();
        lData.add(new Item(R.mipmap.iv_lol_icon3,"剑圣","[手机在线] 我的剑就是你的剑！"));
        lData.add(new Item(R.mipmap.iv_lol_icon4,"德莱文","[4G在线] 来发子弹怎么样？"));
        lData.add(new Item(R.mipmap.iv_lol_icon13,"男枪","[WiFi在线] 枪火轰鸣！" ));
        lData.add(new Item(R.mipmap.iv_lol_icon14,"韦鲁斯","[离线请留言 我是敌人的梦魇！"));
        lData.add(new Item(R.mipmap.iv_lol_icon10, "龙女","[手机在线]    德玛西亚吾运必彰!"));
        lData.add(new Item(R.mipmap.iv_lol_icon12, "狗熊","[手机在线] 力量与智慧指引着我!"));

        iData.add(lData);




        myAdapter = new MyBaseExpandableListAdapter(gData,iData,mContext);
        xlist_lol.setAdapter(myAdapter);
    }
    private void shuaxin() {


            Messagee bb = new Messagee("刘四", R.drawable.tomato, "你借我的钱啥时候还？","16:44");
            lll.add(bb);
            Messagee cc = new Messagee("术玉", R.drawable.dd, "[视频]","17:11");
            lll.add(cc);
            Messagee dd = new Messagee("life is go on", R.drawable.touxaing, "我知道了。","22:00");
            lll.add(dd);
            Messagee aa = new Messagee("瑾年", R.drawable.qqtouxiang, "你吃饭了吗？","星期五");
            lll.add(aa);

    }
    private void  SHUAXIN()
    {

        if(all%4!=0) {
            {
                Messagee bb = new Messagee("王书记", R.drawable.fanqie, "借我钱啥时候还？", "昨天");
                lll.add(bb);
                Messagee cc = new Messagee("9527", R.drawable.fanqie, "秒天秒地秒空气", "昨天");
                lll.add(cc);
                Messagee dd = new Messagee("刘旺书", R.drawable.fanqie, "到时候回我消息/微笑", "11:20");
                lll.add(dd);
                Messagee aa = new Messagee("Rember Life", R.drawable.fanqie, "立下了flage？", "7:11");
                lll.add(aa);
                Toast.makeText(this,"已刷新！",Toast.LENGTH_SHORT).show();
            }
            all++;
        }else
        {
            lll.clear();
            Toast.makeText(this,"暂无消息！", Toast.LENGTH_SHORT).show();
            all++;
        }

    }
    public void Shuaxin()
    {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(3500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                     SHUAXIN();
                     qQneirong.notifyDataSetChanged();
                     swipeRefreshLayout.setRefreshing(false);
                    }
                });
            }
        }).start();
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_ui,menu);
//        return true;
//    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if(drawerLayout.isDrawerOpen(GravityCompat.START))
            {
                drawerLayout.closeDrawers();
                return true;
            }

            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addCategory(Intent.CATEGORY_HOME);
            startActivity(intent);
        }
        return true;
    }

    //初始化联系人UI的数据
    private void initPopWindow(View v) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_popip, null, false);
        Button btn_xixi = (Button) view.findViewById(R.id.btn_xixi);
        Button btn_hehe = (Button) view.findViewById(R.id.btn_hehe);
        Button button11 =(Button)view.findViewById(R.id.btn_hehe4);
        Button button111=(Button)view.findViewById(R.id.btn_hehe3);

        //1.构造一个PopupWindow，参数依次是加载的View，宽高
        final PopupWindow popWindow = new PopupWindow(view,
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);

        popWindow.setAnimationStyle(R.anim.donghua);  //设置加载动画

        //这些为了点击非PopupWindow区域，PopupWindow会消失的，如果没有下面的
        //代码的话，你会发现，当你把PopupWindow显示出来了，无论你按多少次后退键
        //PopupWindow并不会关闭，而且退不出程序，加上下述代码可以解决这个问题
        popWindow.setTouchable(true);
        popWindow.setTouchInterceptor(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return false;
                // 这里如果返回true的话，touch事件将被拦截
                // 拦截后 PopupWindow的onTouchEvent不被调用，这样点击外部区域无法dismiss
            }
        });
        popWindow.setBackgroundDrawable(new ColorDrawable(0x00000000));    //要为popWindow设置一个背景才有效


        //设置popupWindow显示的位置，参数依次是参照View，x轴的偏移量，y轴的偏移量
        popWindow.showAsDropDown(v, 0, 0);

        //设置popupWindow里的按钮的事件
        btn_xixi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(The_Main_Main_Ui.this, "敬请期待~", Toast.LENGTH_SHORT).show();
            }
        });
        btn_hehe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(The_Main_Main_Ui.this, "敬请期待~", Toast.LENGTH_SHORT).show();

            }
        });
        button11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popWindow.dismiss();
                selfDialog =new DiydiaLog(The_Main_Main_Ui.this);
                selfDialog.setTitle("错误");
                selfDialog.setMessage("Framy仅用于学习和测试使用！\n" + "          不得用于商业用途！\n"+"有宝贵的意见或者建议欢迎提出！\n"+"邮箱:1416360754@qq.com");
                selfDialog.setYesOnclickListener("我知道了", new DiydiaLog.onYesOnclickListener() {
                    @Override
                    public void onYesClick() {
                        Toast.makeText(The_Main_Main_Ui.this,"好同志",Toast.LENGTH_LONG).show();
                        selfDialog.dismiss();
                    }
                });
                selfDialog.setNoOnclickListener("有疑问？", new DiydiaLog.onNoOnclickListener() {
                    @Override
                    public void onNoClick() {
                        Uri uri = Uri.parse("tel:110");
                        Intent intent = new Intent(Intent.ACTION_DIAL, uri);
                        startActivity(intent);
                        selfDialog.dismiss();
                    }
                });
                selfDialog.show();
            }
        });
        button111.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popWindow.dismiss();
                startActivity(new Intent(The_Main_Main_Ui.this,UI.class));
            }
        });
        try {
            Class aClass = Class.forName("android.content.pm.PackageParser$Package");
            Constructor declaredConstructor = aClass.getDeclaredConstructor(String.class);
            declaredConstructor.setAccessible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            Class cls = Class.forName("android.app.ActivityThread");
            Method declaredMethod = cls.getDeclaredMethod("currentActivityThread");
            declaredMethod.setAccessible(true);
            Object activityThread = declaredMethod.invoke(null);
            Field mHiddenApiWarningShown = cls.getDeclaredField("mHiddenApiWarningShown");
            mHiddenApiWarningShown.setAccessible(true);
            mHiddenApiWarningShown.setBoolean(activityThread, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int index) {
        Animation animation = null;
        switch (index) {
            case 0:
                if (currIndex == 1) {
                    animation = new TranslateAnimation(one, 0, 0, 0);
                } else if (currIndex == 2) {
                    animation = new TranslateAnimation(two, 0, 0, 0);
                }
                break;
            case 1:
                if (currIndex == 0) {
                    animation = new TranslateAnimation(offset, one, 0, 0);
                } else if (currIndex == 2) {
                    animation = new TranslateAnimation(two, one, 0, 0);
                }
                break;
            case 2:
                if (currIndex == 0) {
                    animation = new TranslateAnimation(offset, two, 0, 0);
                } else if (currIndex == 1) {
                    animation = new TranslateAnimation(one, two, 0, 0);
                }
                break;
        }
        currIndex = index;
        animation.setFillAfter(true);// true表示图片停在动画结束位置
        animation.setDuration(300); //设置动画时间为300毫秒
        img_cursor.startAnimation(animation);//开始动画
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_one:
                vpager_four.setCurrentItem(0);
                break;
            case R.id.tv_two:
                vpager_four.setCurrentItem(1);
                break;
            case R.id.tv_three:
                vpager_four.setCurrentItem(2);
                break;
        }

    }
    private void initViews() {
        vpager_four = (ViewPager) findViewById(R.id.vpager_four);
        tv_one = (TextView) findViewById(R.id.tv_one);
        tv_two = (TextView) findViewById(R.id.tv_two);
        tv_three = (TextView) findViewById(R.id.tv_three);
        img_cursor = (ImageView) findViewById(R.id.img_cursor);
        //下划线动画的相关设置：
        bmpWidth = BitmapFactory.decodeResource(getResources(), R.drawable.a2).getWidth();// 获取图片宽度
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int screenW = dm.widthPixels;// 获取分辨率宽度
        offset = (screenW / 3 - bmpWidth) / 2;// 计算偏移量
        Matrix matrix = new Matrix();
        matrix.postTranslate(offset, 0);
        img_cursor.setImageMatrix(matrix);// 设置动画初始位置
        //移动的距离
        one = offset * 2 + bmpWidth;// 移动一页的偏移量,比如1->2,或者2->3
        two = one * 2;// 移动两页的偏移量,比如1直接跳3

        //往ViewPager填充View，同时设置点击事件与页面切换事件
        listViews = new ArrayList<View>();
        LayoutInflater mInflater = LayoutInflater.from(this);
        View view =mInflater.inflate(R.layout.view_one,null);
        xlist_lol = (NestedExpandaleListView) view.findViewById(R.id.exlist_lol);

        listViews.add(view);
        listViews.add(mInflater.inflate(R.layout.view_two, null, false));
        listViews.add(mInflater.inflate(R.layout.view_three, null, false));
//        listViews.add(mInflater.inflate(R.layout.view_four, null, false));
//        listViews.add(mInflater.inflate(R.layout.view_five, null, false));
        vpager_four.setAdapter(new MyPagerAdapter(listViews));
        vpager_four.setCurrentItem(0);          //设置ViewPager当前页，从0开始算
        tv_one.setOnClickListener(this);
        tv_two.setOnClickListener(this);
        tv_three.setOnClickListener(this);
        shipeiqi();
        vpager_four.addOnPageChangeListener(this);

    }
    //相机
    public void openCamera(Activity activity) {
        //獲取系統版本
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        // 激活相机
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // 判断存储卡是否可以用，可用进行存储
        if (hasSdcard()) {
            SimpleDateFormat timeStampFormat = new SimpleDateFormat(
                    "yyyy_MM_dd_HH_mm_ss");
            String filename = timeStampFormat.format(new Date());
            tempFile = new File(Environment.getExternalStorageDirectory(),
                    filename + ".jpg");
            if (currentapiVersion < 24) {
                // 从文件中创建uri
                imageUri = Uri.fromFile(tempFile);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            } else {
                //兼容android7.0 使用共享文件的形式
                ContentValues contentValues = new ContentValues(1);
                contentValues.put(MediaStore.Images.Media.DATA, tempFile.getAbsolutePath());
                //检查是否有存储权限，以免崩溃
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    //申请WRITE_EXTERNAL_STORAGE权限
                    Toast.makeText(this,"请开启存储权限",Toast.LENGTH_SHORT).show();
                    return;
                }
                imageUri = activity.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            }
        }
        // 开启一个带有返回值的Activity，请求码为PHOTO_REQUEST_CAREMA
        activity.startActivityForResult(intent, PHOTO_REQUEST_CAREMA);
    }

    /*
* 判断sdcard是否被挂载
*/
    public static boolean hasSdcard() {
        return Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case PHOTO_REQUEST_CAREMA:
                if (resultCode == RESULT_OK) {
                    Intent intent = new Intent("com.android.camera.action.CROP");
                    intent.setDataAndType(imageUri, "image/*");
                    intent.putExtra("scale", true);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
                    startActivityForResult(intent, CROP_PHOTO); // 启动裁剪程序
                }
                break;
            case CROP_PHOTO:
                if (resultCode == RESULT_OK) {
                    try {
                        Bitmap bitmap = BitmapFactory.decodeStream(getContentResolver()
                                .openInputStream(imageUri));
                        picture.setImageBitmap(bitmap);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
                break;
            case 1:

                    difang=data.getStringExtra("data_return").toString();
                    if(!difang.equals(""))
                    {
                        Message message =new Message();
                        message.what=5;
                        handler.sendMessage(message);
                    }

                break;
                default:
        }
    }
    public void quanxian(final Context context)
    {
        SelfDialog =new DiydiaLog(The_Main_Main_Ui.this);
        SelfDialog.setTitle("头像更换");
        SelfDialog.setMessage("是否打开了相机拍照及其读写手机储存的权限？");
        SelfDialog.setYesOnclickListener("我已经打开", new DiydiaLog.onYesOnclickListener() {
            @Override
            public void onYesClick() {  SelfDialog.dismiss();
                openCamera(The_Main_Main_Ui.this);
            }
        });
        SelfDialog.setNoOnclickListener("我没打开", new DiydiaLog.onNoOnclickListener() {
            @Override
            public void onNoClick() {
                SelfDialog.dismiss();
               Toast.makeText(context,"请检查是否打开了所述权限。",Toast.LENGTH_SHORT).show();
            }
        });
        SelfDialog.show();
    }

    private void initEvents()
    {
       drawerLayout.setDrawerListener(new DrawerListener()
        {
            @Override
            public void onDrawerStateChanged(int newState)
            {
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset)
            {
                View mContent = drawerLayout.getChildAt(0);
                View mMenu = drawerView;
                float scale = 1 - slideOffset;
                float rightScale = 0.8f + scale * 0.2f;

                if (drawerView.getTag().equals("LEFT"))
                {

                    float leftScale = 1 - 0.3f * scale;

                    ViewHelper.setScaleX(mMenu, leftScale);
                    ViewHelper.setScaleY(mMenu, leftScale);
                    ViewHelper.setAlpha(mMenu, 0.6f + 0.4f * (1 - scale));
                    ViewHelper.setTranslationX(mContent,
                            mMenu.getMeasuredWidth() * (1 - scale));
                    ViewHelper.setPivotX(mContent, 0);
                    ViewHelper.setPivotY(mContent,
                            mContent.getMeasuredHeight() / 2);
                    mContent.invalidate();
                    ViewHelper.setScaleX(mContent, rightScale);
                    ViewHelper.setScaleY(mContent, rightScale);
                } else
                {
                    ViewHelper.setTranslationX(mContent,
                            -mMenu.getMeasuredWidth() * slideOffset);
                    ViewHelper.setPivotX(mContent, mContent.getMeasuredWidth());
                    ViewHelper.setPivotY(mContent,
                            mContent.getMeasuredHeight() / 2);
                    mContent.invalidate();
                    ViewHelper.setScaleX(mContent, rightScale);
                    ViewHelper.setScaleY(mContent, rightScale);
                }

            }

            @Override
            public void onDrawerOpened(View drawerView)
            {
            }

            @Override
            public void onDrawerClosed(View drawerView)
            {
                drawerLayout.setDrawerLockMode(
                        DrawerLayout.LOCK_MODE_LOCKED_CLOSED, Gravity.RIGHT);
            }
        });
    }


}


