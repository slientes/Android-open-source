package com.example.qq.qidong;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.example.qq.H5.MainActivity;
import com.example.qq.R;
import com.example.qq.The_Main_Main_Ui;

public class qidong extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_qidong);
        ActionBar bar = getSupportActionBar();
          if(bar!=null)
          {
              bar.hide();
          }
          new Thread(new Runnable() {
              @Override
              public void run() {
                  try{
                      Thread.sleep(2500);
                      Intent intent = new Intent(qidong.this,The_Main_Main_Ui.class);
                      startActivity(intent);
                      finish();
                  }catch (Exception e)
                  {

                      e.printStackTrace();
                  }

              }
          }).start();
    }
}
