package com.example.qq.Adapter_for_youhu;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.qq.R;
import com.example.qq.The_three_adapter.Three;

import java.util.List;

/**
 * Created by 烬 on 2018/6/30.
 */

public class Myyouhua extends RecyclerView.Adapter<Myyouhua.ViewHolder> {
    private List<right_list> Right_list;
    private OnClickItemListener onClickItemListener1;

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textView;
        RelativeLayout relativeLayout;
        public ViewHolder(View view)
        {
            super(view);
            imageView =view.findViewById(R.id.Image_R);
            textView  =view.findViewById(R.id.Text_R);
            relativeLayout=view.findViewById(R.id.RRRRR);

        }
    }
    public Myyouhua(List<right_list> three)
    {
        this.Right_list=three;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.youhua_item,parent,false);
        ViewHolder holder =new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        right_list right_list1=Right_list.get(position);
       holder.textView.setText(right_list1.getName());
       holder.imageView.setImageResource(right_list1.getImageId());

        if(onClickItemListener1!=null)
        {
            holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int poss = holder.getAdapterPosition();
                    onClickItemListener1.onClick(holder.relativeLayout,poss);
                }
            });

        }
    }
    @Override
    public int getItemCount() {
        return Right_list.size();
    }
    public interface OnClickItemListener{
        void onClick(View view,int pos);

    }
    public void SetOnClickItemListener(OnClickItemListener onClickItemListener)
    {
        this.onClickItemListener1 =onClickItemListener;
    }
}