package com.example.qq.The_three_adapter;

import com.example.qq.The_Main_Main_Ui;

/**
 * Created by 烬 on 2018/6/9.
 */

public class Three {
    private String name;
    private int imageId1;
    private int imageId2;
    public Three(int imageId1,String name,int imageId2)
    {
        this.imageId1=imageId1;
        this.imageId2=imageId2;
        this.name=name;

    }

    public void setImageId2(int imageId2) {
        this.imageId2 = imageId2;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImageId2() {
        return imageId2;
    }

    public void setImageId1(int imageId1) {
        this.imageId1 = imageId1;
    }

    public String getName() {
        return name;
    }

    public int getImageId1() {
        return imageId1;
    }
}
