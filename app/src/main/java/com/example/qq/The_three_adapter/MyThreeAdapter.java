package com.example.qq.The_three_adapter;

import android.provider.ContactsContract;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.ThemedSpinnerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.qq.R;
import com.example.qq.The_Main_Main_Ui;

import java.util.List;

/**
 * Created by 烬 on 2018/6/9.
 */

public class MyThreeAdapter extends RecyclerView.Adapter<MyThreeAdapter.ViewHolder> {
    private List<Three> three;
    private OnClickItemListener onClickItemListener;
    public void Yichu(int pos)
    {
        three.remove(pos);
        notifyItemRemoved(pos);
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView1;
        ImageView imageView2;
        TextView textView;
        RelativeLayout linearLayout;
        public ViewHolder(View view)
        {
            super(view);
            imageView1=(ImageView)view.findViewById(R.id.A1);
            textView =(TextView)view.findViewById(R.id.A2);
            imageView2 =(ImageView)view.findViewById(R.id.A3);
            linearLayout=(RelativeLayout)view.findViewById(R.id.zaizai);
        }
    }
    public MyThreeAdapter(List<Three> three)
    {
        this.three=three;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.the_three,parent,false);
        ViewHolder holder =new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Three mthree = three.get(position);
        holder.imageView1.setImageResource(mthree.getImageId1());
        holder.imageView2.setImageResource(mthree.getImageId2());
        holder.textView.setText(mthree.getName());

        if(onClickItemListener!=null)
        {
            holder.linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int poss = holder.getAdapterPosition();
                    onClickItemListener.onClick(holder.linearLayout,poss);
                }
            });
            holder.linearLayout.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    int poss =  holder.getAdapterPosition();
                    onClickItemListener.onLongClick(holder.linearLayout,poss);
                    return false;
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return three.size();
    }
    public interface OnClickItemListener{
        void onClick(View view,int pos);
        void onLongClick(View view,int pos);
    }
    public void SetOnClickItemListener(OnClickItemListener onClickItemListener)
    {
        this.onClickItemListener =onClickItemListener;
    }
}
