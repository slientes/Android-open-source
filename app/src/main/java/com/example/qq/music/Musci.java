package com.example.qq.music;

import android.Manifest;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.qq.R;

import java.io.File;

public class Musci extends AppCompatActivity implements View.OnClickListener{
    private MediaPlayer mediaPlayer = new MediaPlayer();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_musci);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        Button kaishi = (Button)findViewById(R.id.button22);
        Button zanting = (Button)findViewById(R.id.button33);
        Button jieshu = (Button)findViewById(R.id.button44);
        kaishi.setOnClickListener(this);
        zanting.setOnClickListener(this);
        jieshu.setOnClickListener(this);
        if(ContextCompat.checkSelfPermission(Musci.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(Musci.this,new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE},1);
        }
        else{
            startPlay();
        }
    }
private void startPlay(){
        try{
            File file = new File(Environment.getExternalStorageDirectory(),"病态患者.mp3");
            mediaPlayer.setDataSource(file.getPath());
            mediaPlayer.prepare();
        }catch (Exception e)
        {
            e.getMessage();
        }
}
@Override
public void onRequestPermissionsResult(int requestCode,String[] permissions,int[] grantResults)
{
    switch (requestCode)
    {
        case 1:
            if (grantResults.length>0  &&  grantResults[0]==PackageManager.PERMISSION_GRANTED)
        {
              startPlay();
        }else
            {
                Toast.makeText(this,"你拒绝了该权限，此功能将无法和使用！",Toast.LENGTH_SHORT).show();
                this.finish();
            }


    }
}
    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.button22:
                if(!mediaPlayer.isPlaying())
                {
                    mediaPlayer.start();
                    Toast.makeText(this,"开始播放！",Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.button33:
                if(mediaPlayer.isPlaying())
                {
                    mediaPlayer.pause();
                    Toast.makeText(this,"暂停播放！",Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.button44:
                if(mediaPlayer.isPlaying())
                {
                    mediaPlayer.reset();
                    Toast.makeText(this,"结束播放，设备进入准备状态！",Toast.LENGTH_SHORT).show();
                    startPlay();
                }
                break;
            default:
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mediaPlayer!=null)
        {
            mediaPlayer.stop();
            mediaPlayer.release();

        }
    }
}
