package com.example.qq.H5;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

import com.example.qq.Diy.DiydiaLog;
import com.example.qq.Diy.MyToast;


/**
 * Created by Monster on 2018/10/26.
 */
public class JavaScriptinterface {
    private DiydiaLog d;
    Context context;
    public JavaScriptinterface(Context c) {
        context= c;
    }

    /**
     * 与js交互时用到的方法，在js里直接调用的
     */
    @JavascriptInterface
    public void showToast(String ssss) {
        d =new DiydiaLog(this.context);
       d.setTitle("温馨提示" +
               "");
        d.setMessage(ssss);
        d.setYesOnclickListener("好的", new DiydiaLog.onYesOnclickListener() {
            @Override
            public void onYesClick() {

                MyToast.DiyToast(context, "好同志！");
                d.dismiss();
            }
        });

        d.show();
    }

}