package com.example.qq.Read_People_of_Phone;

import android.Manifest;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.qq.R;
import com.example.qq.The_Main_Main_Ui;

import java.util.ArrayList;
import java.util.List;

public class readdd extends AppCompatActivity {
    List<String> list = new ArrayList<>();
    ArrayAdapter<String> arrayAdapter;
    Context context;
    Cursor _cursor = null;
    private int sum =0;
     long[] phonenumber =new long[100];
    long   f;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read2);
//        _cursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,null,null,null);
        final ClipboardManager myClipboard=  (ClipboardManager)getSystemService(CLIPBOARD_SERVICE);
//         for(int a=0;a<=list.size();a++)
//         {
//             try{
//                 if(_cursor!=null)
//                 {
//                     while(_cursor.moveToNext())
//                     {
//                         String number = _cursor.getString(_cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
//                         long   f   =  Long.parseLong(number);
//                         phonenumber[sum]=f;
//                         sum++;
//                     }
//                     arrayAdapter.notifyDataSetChanged();
//                 }
//             }catch (Exception e)
//             {
//                 e.printStackTrace();
//             }finally {
//                 if(_cursor!=null)
//                 {
//                     _cursor.close();
//                 }
//             }
//         }
        ListView listView = (ListView)findViewById(R.id.Reccy);
        arrayAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,list);
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ClipData myClip;
                myClip = ClipData.newPlainText("text", phonenumber[position]+"");
                myClipboard.setPrimaryClip(myClip);
                Toast.makeText(readdd.this,"已复制到剪切板",Toast.LENGTH_SHORT).show();
            }

        });

        if(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS)!= PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.READ_CONTACTS},1);
        }
        else {
            read();
        }
        Toast.makeText(readdd.this,"点击可复制其电话号码",Toast.LENGTH_LONG).show();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode)
        {
            case 1:
                if(grantResults.length>0 && grantResults[0]==PackageManager.PERMISSION_GRANTED){

                    read();

                }else
                {
                    Toast.makeText(this,"发生未知的错误！",Toast.LENGTH_SHORT).show();

                }
                break;
            default:
        }
    }

    private void read() {
        Cursor cursor = null;
        try{
            cursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,null,null,null);
            if(cursor!=null)
            {
                while(cursor.moveToNext())
                {
                    String name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                    String number = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                     f   =  Long.parseLong(stringToNumber(number));
                    phonenumber[sum]=f;
                    sum++;
                    list.add(name+"  " + number);

                }
                arrayAdapter.notifyDataSetChanged();
            }                                   //  why?????????????????????????????????????  
        }catch (Exception e)
        {
            e.printStackTrace();
        }finally {
            if(cursor!=null)
            {
                cursor.close();
            }
        }
    }

    public String stringToNumber (String string) {
        string = string.trim();
        String string1 = "";
        if (string != null && !string.equals("")) {
            for (int i = 0; i < string.length(); i++) {
                if (string.charAt(i) >= 48 && string.charAt(i) <= 57) {
                    string1 += string.charAt(i);
                }
            }
        }
        return string1;
    }
}
