package com.example.qq.gps;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.view.View;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.SDKInitializer;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.MapStatus;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.MyLocationData;
import com.baidu.mapapi.model.LatLng;
import com.example.qq.R;

import java.util.ArrayList;
import java.util.List;

public class Map extends AppCompatActivity {
    public LocationClient mlocationClient;
    private TextView textView;
    private MapView mapView;
    private BaiduMap baiduMap;
    private Boolean is = true;
//    public Button button, button1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mlocationClient = new LocationClient(getApplicationContext());
        mlocationClient.registerLocationListener(new My());
        SDKInitializer.initialize(getApplicationContext());
        setContentView(R.layout.activity_map);
        mapView=(MapView)findViewById(R.id.map);
        baiduMap=mapView.getMap();
        baiduMap.setMyLocationEnabled(true);
        start();
//        textView = (TextView) findViewById(R.id.web);
//        List<String> list = new ArrayList<>();
//        if (ContextCompat.checkSelfPermission(Map.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            list.add(Manifest.permission.ACCESS_FINE_LOCATION);
//        }
//        if (ContextCompat.checkSelfPermission(Map.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
//            list.add(Manifest.permission.READ_PHONE_STATE);
//        }
//        if (ContextCompat.checkSelfPermission(Map.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
//            list.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
//        }
//        if (!list.isEmpty()) {
//            String[] List = list.toArray(new String[list.size()]);
//            ActivityCompat.requestPermissions(Map.this, List, 1);
//        } else {
////          start();
//        }
    }
private void navigateTo(BDLocation location){
        if(is){
            LatLng ll = new LatLng(location.getLatitude(),location.getLongitude());
            MapStatusUpdate update = MapStatusUpdateFactory.newLatLng(ll);
            baiduMap.animateMapStatus(update);
            update = MapStatusUpdateFactory.zoomTo(16f);
            baiduMap.animateMapStatus(update);
            is=false;
        }
    MyLocationData.Builder builder = new MyLocationData.Builder();
        builder.latitude(location.getLatitude());
        builder.longitude(location.getLongitude());
        MyLocationData locationData = builder.build();
        baiduMap.setMyLocationData(locationData);
}
    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
    }

    private void start() {
        shuaxin();
        mlocationClient.start();
    }

    private void shuaxin() {
        LocationClientOption option = new LocationClientOption();
        option.setScanSpan(5000);
        option.setIsNeedAddress(true);
        mlocationClient.setLocOption(option);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mlocationClient.stop();
        mapView.onDestroy();
        baiduMap.setMyLocationEnabled(false);
    }


//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        switch (requestCode) {
//            case 1:
//                if (grantResults.length > 0) {
//                    for (int result : grantResults) {
//                        if (result != PackageManager.PERMISSION_GRANTED) {
//                            Toast.makeText(Map.this, "必须授权所有通知才可以使用GPS！", Toast.LENGTH_SHORT).show();
//                            finish();
//                            return;
//                        }
//                    }
//                    start();
//                } else {
//                    Toast.makeText(Map.this, "发生未知的错误！", Toast.LENGTH_SHORT).show();
//                    finish();
//                }
//                break;
//
//            default:
//        }
//    }
    public  class My implements BDLocationListener{

        @Override
        public void onReceiveLocation(BDLocation Location) {
//            StringBuilder d = new StringBuilder();
//            d.append("纬度：").append(bdLocation.getLatitude()).append("\n");
//            d.append("经度：").append(bdLocation.getLongitude()).append("\n");
//            d.append("国家：").append(bdLocation.getCountry()).append("\n");
//            d.append("省份：").append(bdLocation.getProvince()).append("\n");
//            d.append("市区：").append(bdLocation.getCity()).append("\n");
//            d.append("地区：").append(bdLocation.getDistrict()).append("\n");
//            d.append("街道：").append(bdLocation.getStreet()).append("\n");
//            d.append("定位方式：");
//            if(bdLocation.getLocType()==BDLocation.TypeGpsLocation){
//                d.append("GPS");
//            }else if(bdLocation.getLocType()==BDLocation.TypeNetWorkException)
//            {
//                d.append("网络");
//            }else
//                d.append("未知");

            if(Location.getLocType()==BDLocation.TypeGpsLocation || Location.getLocType()==BDLocation.TypeNetWorkLocation)
       {
                  navigateTo(Location);
            }
        }
    }
}


