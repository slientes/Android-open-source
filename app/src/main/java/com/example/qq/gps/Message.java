package com.example.qq.gps;

/**
 * Created by Monster on 2018/11/9.
 */

public class Message {
    public String riqi;
    public String gaowen;
    public String fengli;
    public String diwen;
    public String fengxiang;
    public String tianqi;
    public Message(){}
    public String getRiqi() {
        return riqi;
    }
    public void setRiqi(String riqi) {
        this.riqi = riqi;
    }
    public String getGaowen() {
        return gaowen;
    }
    public void setGaowen(String gaowen) {
        this.gaowen = gaowen;
    }
    public String getFengli() {
        return fengli;
    }
    public void setFengli(String fengli) {
        this.fengli = fengli;
    }
    public String getDiwen() {
        return diwen;
    }
    public void setDiwen(String diwen) {
        this.diwen = diwen;
    }
    public String getFengxiang() {
        return fengxiang;
    }
    public void setFengxiang(String fengxiang) {
        this.fengxiang = fengxiang;
    }
    public String getTianqi() {
        return tianqi;
    }
    public void setTianqi(String tianqi) {
        this.tianqi = tianqi;
    }
}
