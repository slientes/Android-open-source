package com.example.qq.setui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.qq.Diy.DiydiaLog;
import com.example.qq.R;
import com.example.qq.The_Main_Main_Ui;

public class ScrollingActivity extends AppCompatActivity {
    private long firstTime = 0;
    private DiydiaLog selfDialog;



    //1.定义不同颜色的菜单项的标识:
    final private int RED = 110;
    final private int GREEN = 111;
    final private int BLUE = 112;
    final private int YELLOW = 113;
    final private int GRAY= 114;
    final private int CYAN= 115;
    final private int BLACK= 116;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.design.widget.CollapsingToolbarLayout collapsingToolbarLayout =(CollapsingToolbarLayout)findViewById(R.id.toolbar_layout);
        collapsingToolbarLayout.setTitle("Framy 设置");
        RelativeLayout relativeLayout =(RelativeLayout)findViewById(R.id.RE1);
        RelativeLayout relativeLayout1=(RelativeLayout)findViewById(R.id.RE2);
        RelativeLayout relativeLayout2=(RelativeLayout)findViewById(R.id.RE3);
        RelativeLayout relativeLayout3 =(RelativeLayout)findViewById(R.id.RE11);
        RelativeLayout relativeLayout4=(RelativeLayout)findViewById(R.id.RE22);
        RelativeLayout relativeLayout5=(RelativeLayout)findViewById(R.id.RE33);
        RelativeLayout relativeLayout6 =(RelativeLayout)findViewById(R.id.RE111);
        RelativeLayout relativeLayout7=(RelativeLayout)findViewById(R.id.RE222);
        RelativeLayout relativeLayout8=(RelativeLayout)findViewById(R.id.RE333);
        RelativeLayout relativeLayout9=(RelativeLayout)findViewById(R.id.RR4);
        RelativeLayout relativeLayout10=(RelativeLayout)findViewById(R.id.RR5);
        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"ddd",Toast.LENGTH_SHORT).show();
            }
        });
        relativeLayout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"敬请期待",Toast.LENGTH_SHORT).show();

            }
        });
        relativeLayout2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"敬请期待",Toast.LENGTH_SHORT).show();

            }
        });
        relativeLayout3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"敬请期待",Toast.LENGTH_SHORT).show();

            }
        });  relativeLayout4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"敬请期待",Toast.LENGTH_SHORT).show();

            }
        });  relativeLayout5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"敬请期待",Toast.LENGTH_SHORT).show();

            }
        });  relativeLayout6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"敬请期待",Toast.LENGTH_SHORT).show();
            }
        });  relativeLayout7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"敬请期待",Toast.LENGTH_SHORT).show();

            }
        });  relativeLayout8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"敬请期待",Toast.LENGTH_SHORT).show();

            }
        });  relativeLayout9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"敬请期待",Toast.LENGTH_SHORT).show();
            }

        });
        relativeLayout10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selfDialog =new DiydiaLog(ScrollingActivity.this);
                selfDialog.setTitle("Framy V2.1.0");
                selfDialog.setMessage("Framy仅用于学习和测试使用！\n" + "          不得用于商业用途！\n"+"有宝贵的意见或者建议欢迎提出！\n"+"邮箱:1416360754@qq.com");
                selfDialog.setYesOnclickListener("我知道了", new DiydiaLog.onYesOnclickListener() {
                    @Override
                    public void onYesClick() {
                        Toast.makeText(ScrollingActivity.this,"好同志",Toast.LENGTH_LONG).show();
                        selfDialog.dismiss();
                    }
                });
                selfDialog.setNoOnclickListener("有疑问？", new DiydiaLog.onNoOnclickListener() {
                    @Override
                    public void onNoClick() {
                        Uri uri = Uri.parse("tel:110");
                        Intent intent = new Intent(Intent.ACTION_DIAL, uri);
                        startActivity(intent);
                        selfDialog.dismiss();
                    }
                });
                selfDialog.show();
            }
        });
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                long secondTime = System.currentTimeMillis();
                if (secondTime - firstTime > 2000) {
                    Snackbar.make(view, "双击退出", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    firstTime = secondTime;
                } else {
                    finish();
                }

            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        menu.add(2,RED,4,"红色");
        menu.add(1,GREEN,2,"绿色");
        menu.add(1,BLUE,3,"蓝色");
        menu.add(1,YELLOW,1,"黄色");
        menu.add(1,GRAY,5,"灰色");
        menu.add(1,CYAN,6,"蓝绿色");
        menu.add(1,BLACK,7,"黑色");
        return true;
    }

}
